{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "535445d2-9a14-487d-b12e-1c45651b68ae",
   "metadata": {},
   "source": [
    "# Random numbers\n",
    "\n",
    "An important part of any simulation is an element of randomness. If, for example, there are 10,000 possible things that can happen in your simulation (e.g. genes to mutate or molecules to interact) you want to understand the effects of as many of them as possible. One way to do that is to try them one at a time, starting with the first and going through the list. This might be impractical due to it taking too much time, computer memory, etc.\n",
    "\n",
    "Another option is to \"sample\" from the possible options, e.g. just try out 100 of them. This will take less time but you must be careful how you choose which ones to try. A naïve approach would be to do the first 100 in the list, but this will very likely lead to a bias (perhaps all the genes controlling movement are near the beginning of the genome).\n",
    "\n",
    "The best way to avoid these biases is to select your sample as randomly as possible."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "172ba275-a9a5-4bcc-84a1-9c7d1a35d603",
   "metadata": {},
   "source": [
    "numpy provides lots of very good tools for generating random numers, depending on exactly what you want to use them for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "6219d874-774e-4fbb-9d39-9ce7548007ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy.random import default_rng\n",
    "\n",
    "rng = default_rng()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6139f7b-5355-4ff0-be4a-9d51ddf4e5aa",
   "metadata": {
    "tags": []
   },
   "source": [
    "This [`rng` is our \"random number generator\"](https://numpy.org/doc/stable/reference/random/generator.html). Depending on what we want from it, it can provide randomness in lots of different ways.\n",
    "\n",
    "The simplest is the [`random` method](https://numpy.org/doc/stable/reference/random/generated/numpy.random.Generator.random.html) it provides:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "68fb5920-70c2-4a01-b64d-2b86182897ee",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.8153786322096002"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.random()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed179458-4a7a-4ef4-b7d1-80d869288984",
   "metadata": {},
   "source": [
    "You will get a different number to what I have here and if you rerun the line of code, it will keep giving you different values. They will always be $0 \\le r \\lt 1.0$ and should sample over that range evenly."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91db6cfd-cf1b-4e89-95d6-4a839fee59f6",
   "metadata": {},
   "source": [
    "As we saw with `np.zeros` and `np.ones` in the last chapter, you can request an array of random numbers by passing in a `size` argument. So to get three random numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "f6ba8750-ed96-4f70-8e4e-a7c914983602",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0.78074909, 0.98814236, 0.24077928])"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.random(size=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ac32862-e42c-4303-bd16-9086ef5607c1",
   "metadata": {},
   "source": [
    "or even a $2\\times4$ grid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "dd9ec07e-47a0-4281-b3ad-56fb75cd5709",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.29462796, 0.88065677, 0.93041109, 0.26346213],\n",
       "       [0.48620935, 0.8908907 , 0.75843885, 0.6226575 ]])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.random(size=(2, 4))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "372756cd-5b90-4137-9f25-f8e59756836e",
   "metadata": {},
   "source": [
    "If you want integer values instead then there is an [`integers`  method](https://numpy.org/doc/stable/reference/random/generated/numpy.random.Generator.integers.html) which give you integers in the range from `0` to the value you specify:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "cad742d0-dc86-4768-ac44-36df4422ab4b",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "19"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.integers(20)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a45c952-5e1d-4488-9406-06f58edfa775",
   "metadata": {},
   "source": [
    "Or again, asking for 10 of them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "c640534a-2c9e-4f97-9bd1-ee80175f92b4",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 3, 18,  4,  3, 12, 16, 19,  8,  1,  0])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.integers(20, size=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "321423c6-0d3c-4f1e-bf46-976b71395df4",
   "metadata": {},
   "source": [
    "You can also specify the lower bound explicitly, so to get numbers between `4` and `6`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "ac45d1ae-b8c9-4017-982d-48c4d256665a",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([5, 5, 5, 5, 5, 4, 5, 4, 5, 5])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.integers(low=4, high=6, size=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "350c332d-b3b4-40e3-8770-d0039e94bbeb",
   "metadata": {},
   "source": [
    "You'll notice that you'll always get only `4` or `5`. By default the `integers` method doesn't include the upper bound (just like with `range()`). You can tell it to include it by setting `endpoint=True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "5dee2a3d-c6b6-497e-8576-e01ce0b04ea4",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([5, 4, 4, 5, 6, 5, 4, 4, 5, 5])"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rng.integers(low=4, high=6, endpoint=True, size=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6da737e5-4066-479c-aff6-64902f51775f",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 1\n",
    "\n",
    "Generate an array with 3 rows and 5 columns, containing random integers which are $-5 < r <= 8$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06ad13cd-92b7-4344-a586-2c416088e27b",
   "metadata": {},
   "source": [
    "## Pseudo-randomness\n",
    "\n",
    "Random number generators in computers work by implementing an algorithm. Algorithms like this are purely *deterministic*, that is, if you start with the same input and run them many times, they'll always give you the same output. This is because at their core they are just performing mathematical operations to numbers. For this reason, they are often referred to as \"*pseudo*random number generators\".\n",
    "\n",
    "We can see this action by explicitly setting the place from which the random number generator should start with the *seed*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "65e616d1-3b27-4361-9793-dfdb6ba165a1",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0.77395605, 0.43887844, 0.85859792, 0.69736803, 0.09417735])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "seeded_rng_1 = default_rng(seed=42)\n",
    "seeded_rng_1.random(size=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da17ab6d-b55a-4784-95f4-f8d0bcb3e589",
   "metadata": {},
   "source": [
    "If we make a second random number generator, but set the same seed, we will see exactly the same \"random\" numbers being produced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "4323dd09-7200-4131-a28c-624a0fddb367",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0.77395605, 0.43887844, 0.85859792, 0.69736803, 0.09417735])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "seeded_rng_2 = default_rng(seed=42)\n",
    "seeded_rng_2.random(size=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e284b01-bc1b-40b9-ae23-42998ec7dc96",
   "metadata": {},
   "source": [
    "If we set the seed to a different value, then we will get a different sequence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "53910f85-fbf8-41b8-b271-f24af38fe457",
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0.32697228, 0.98727684, 0.31871084, 0.78854894, 0.86989651])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "seeded_rng_3 = default_rng(seed=8)\n",
    "seeded_rng_3.random(size=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93878045-aae7-424b-9726-ea60d859dd57",
   "metadata": {},
   "source": [
    "If we do not set any seed, then the operating system will provide a random starting point for us."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3780c92-a5f7-470c-acd3-197b9733fa42",
   "metadata": {},
   "source": [
    "Having a fixed seed is useful if we want random-type behaviour but also *repeatability*. This is particularly useful when debugging your code."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
